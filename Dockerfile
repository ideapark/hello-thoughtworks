FROM golang:alpine as builder
RUN mkdir /build
ADD . /build/
WORKDIR /build
RUN go build -o /build/hello main.go

FROM alpine
COPY --from=builder /build/hello /app/
WORKDIR /app
USER 1001
EXPOSE 8080
CMD ["./hello"]
