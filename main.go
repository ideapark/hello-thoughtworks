package main

import (
	"log"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	log.Println(r.RemoteAddr)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("hello, thoughtworks!"))
}

func main() {
	http.HandleFunc("/", hello)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
